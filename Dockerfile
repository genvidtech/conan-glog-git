# Dockerfile for building gvencode and composed.
FROM alpine:3.5 AS builder

CMD ["sh"]

RUN mkdir /build

RUN apk add --no-cache \
	cmake \
	coreutils \
	g++ \
	git \
    libunwind-dev \
	linux-headers \
	make \
	makedepend \
	python \
	py2-pip

# Python2 is used because of some crash with python 3 on alpine.
RUN pip install -U pip conan==0.25.1 && \
	conan remote add genvidtech https://conan.genvidtech.net False && \
	conan config set settings_defaults.compiler.libcxx=libstdc++11 && \
	conan config get

COPY . /src

RUN cd src && conan test_package
