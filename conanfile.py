from conans import ConanFile, CMake, tools
import os
import glob

class GlogConan(ConanFile):
    name = "glog"
    version = "0.4.0"
    changelist = "v0.4.0"
    license = "Public Domain License"
    description = "Conan GLog with support for Windows."
    url = "https://bitbucket.org/genvidtech/conan-glog"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "gflags": [True,False]}
    default_options = "shared=False", "gflags=False"
    generators = "cmake"

    def source(self):
        self.run("git clone https://github.com/google/glog.git")
        self.run("cd glog && git checkout %s" % self.changelist)
        #os.remove("glog/test-driver") # A symlink leading to a permission error when copying the source dir for building.
        tools.replace_in_file("glog/CMakeLists.txt",
                              "project(glog VERSION 0.4.0 LANGUAGES C CXX)",
                              """project(glog VERSION 0.4.0 LANGUAGES C CXX)
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()
""")
        # For alpine, the unwind library also need libexecinfo to link properly.
        tools.replace_in_file("glog/CMakeLists.txt",
                              "target_link_libraries (glog PUBLIC ${UNWIND_LIBRARY})",
                              "target_link_libraries (glog PUBLIC ${UNWIND_LIBRARY} execinfo)")


    def build(self):
        cmake = CMake(self)
        shared = "-DBUILD_SHARED_LIBS=ON" if self.options.shared else ""
        gflags = "-DWITH_GFLAGS=OFF" if not self.options.gflags else ""
        opts = " ".join((shared, gflags))
        self.run('cmake glog %s %s' % (cmake.command_line, opts))
        self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        headers = (
            "logging.h",
            "stl_logging.h",
            "raw_logging.h",
            "vlog_is_on.h",
        )
        src = "glog/src/windows/glog" if self.settings.os == "Windows" else "glog"
        for header in headers:            
            self.copy(header, dst="include/glog", src=src)
        self.copy("log_severity.h", dst="include/glog", src="glog/src/glog")
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*glog.lib", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["glog"]
        if not self.options.shared:
            self.cpp_info.defines = ["GOOGLE_GLOG_DLL_DECL="]
